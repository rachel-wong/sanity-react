import React, { useState, useEffect } from 'react'
import BlockContent from '@sanity/block-content-to-react'
import { useParams, useHistory } from 'react-router-dom'
import sanityClient from '../client'
import UrlFor from './util/ImageBuilder.js'
import { Loader } from '../components'

const Project = () => {
  let { slug } = useParams();

  const history = useHistory();

  const [singleProject, setSingleProject] = useState(null)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    sanityClient
      .fetch(`*[slug.current == "firstproject"]{
      title,
      mainImage{
        asset->{
          url
        }
      },
      body,
      publishedAt,
      tags,
      author
    }`)
      .then(res => {
        setSingleProject(res[0])
        setLoading(false) // resetting loading MUST come AFTER hydrating component with data
      })
      .catch(err => console.error(err))
  }, [slug])

  return (
    <>
    { loading ? <Loader /> : (
      <div className="profile-container container">
        <h1 className="profile-heading serif">{singleProject.title}</h1>
        <div className="profile-image-wrapper">
          <img className="profile-image" src={UrlFor(singleProject.mainImage).width(800).url()} alt="" />
        </div>
        <BlockContent blocks={singleProject.body} className="profile-summary san-serif" projectId="" dataset="production"></BlockContent>
        <div className="goback-wrapper">
          <button className="btn goback" onClick={() => history.goBack()}>Back to Projects</button>
        </div>
      </div>
      )}
    </>
  )
}

export default Project