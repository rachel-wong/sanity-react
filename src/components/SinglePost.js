import React, { useState, useEffect } from 'react'
import sanityClient from '../client'
import { useParams } from 'react-router-dom'
import BlockContent from '@sanity/block-content-to-react'
import imageUrlBuilder from '@sanity/image-url'
import { Loader } from "../components"

const SinglePost = () => {

  let { slug } = useParams(); // slug must be destructured from useParams. It can't simply be defined
  const [loading, setLoading] = useState(true)
  const [ blogPost, setBlogPost ] = useState(null)

  const imageBuilder = imageUrlBuilder(sanityClient)

  function urlFor(source) {
    return imageBuilder.image(source)
  }

  useEffect(() => {
    sanityClient.fetch(`*[slug.current == "${slug}" && _type == "post"] {
          title,
          _id,
          slug,
          body,
          publishedAt,
          mainImage{
            asset->{
              _id,
              url
            },
            alt
          },
          "name": author->name
       }`)
      .then(res => {
        setBlogPost(res[0])
        setLoading(false)
      })
      .catch(err => {console.error(err)})
  }, [slug])


  return (
    <>
      {loading ? <Loader /> : (
        <div className="post-container">
          <h1 className="post-title serif">{blogPost.title}</h1>
          <div className="post-image-wrapper">
          <img src={urlFor(blogPost.mainImage).width(600).url()} alt={ blogPost.title}/>
          </div>
          <div className="post-details san-serif">
          <p className="post-date">{ new Date(blogPost.publishedAt).toDateString()}</p>
            <p className="post-author">{blogPost.name ? blogPost.name : "anon"}</p>
          </div>
          <BlockContent className="san-serif post-text" blocks={ blogPost.body } dataset="production" projectId="y3u1a6rx" />
        </div>
      )}
    </>
  )
}

export default SinglePost