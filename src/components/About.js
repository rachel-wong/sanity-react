import React, { useState, useEffect } from 'react'
import BlockContent from '@sanity/block-content-to-react'
import sanityClient from '../client'
import { Loader } from '../components'

const About = () => {
  const [author, setAuthor] = useState(null)
  const [loading, setLoading] = useState(true) // by default starting state, it IS loading

  useEffect(() => {
    sanityClient.fetch(`*[_type == "author"]{
      name,
      bio,
      "authorImage": image.asset->url
    }`)
      .then(res => {
        setAuthor(res[0])
        setLoading(false)
      })
      .catch(err => console.error(err))
  }, [])

  return (
    <>
      {loading ? <Loader /> : (
        <div className="about container">
          <div className="about-col">
            <h1 className="about-heading serif">About {author.name}</h1>
            <div className="about-image-wrapper">
              <img src={author.authorImage} alt="" />
            </div>
          </div>
          <BlockContent className="san-serif about-text" blocks={ author.bio} dataset="production" projectId="y3u1a6rx" />
        </div>
      )}
    </>
  )
}

export default About
