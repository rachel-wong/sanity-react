import React from 'react'

const Home = () => {
  return (
    <div className="home-container pattern-bg">
      <h1 className="home-name serif">Sanity Blog</h1>
      <p className="home-byline san-serif">Testing headless CMS with React</p>
    </div>
  )
}

export default Home