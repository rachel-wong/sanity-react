import React, { useEffect, useState } from 'react'
import sanityClient from '../client'
import { Link } from 'react-router-dom'
import { Loader } from '../components'

const Projects = () => {

  const [projectData, setProjectData] = useState(null)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    sanityClient.fetch(`*[_type == "project"]{
      title,
      slug,
      blurbImage{
        asset->{
          _id,
          url
        },
        alt
      },
      body,
      projectType,
      link,
      tags,
      publishedAt
    }`).then(res => {
      setProjectData(res)
      setLoading(false)
    }).catch(err => {
      console.error(err)
    })
  }, [])

  return (
    <>
    { loading ? <Loader /> : (
    <div className="projects">
      <h1 className="serif projects-heading">Projects</h1>
      <span className="san-serif">A selection of side projects and commercial projects. </span>
      <div className="projects-container">
      { projectData && projectData
        .sort((a, b) => { return new Date(a.publishedAt) - new Date(b.publishedAt) })
        .map((project, idx) => (
          <Link className="single-project-container" to={"/projects/" + project.slug.current} key={ project.slug.current } >
          <div className="container mx-auto san-serif" key={ idx }>
              <h1 className="project-title">{project.title}</h1>
              <div className="project-image-wrapper">
                <img src={project.blurbImage.asset.url} alt={project.title} />
              </div>
              <div className="project-details">
              <p>Project Type: { project.projectType }</p>

              { project.tags.length > 0 ? (
                <ul className="project-tags">
                      {project.tags.map((tag, idx) => (
                        <li className="project-tag" key={idx}>{tag}</li>
                      ))}
                </ul>
                ) : null}
            </div>
            <a href={ project.link }>{ project.link }</a>
            <p>Published at: { new Date(project.publishedAt).toDateString() }</p>
            </div>
          </Link>
        )
        )}
        </div>
        </div>
      )}
    </>
  )
}

export default Projects