import sanityClient from '../../client'
import imageUrlBuilder from '@sanity/image-url'

const UrlFor = function(source) {
    const imageBuilder = imageUrlBuilder(sanityClient)

    return (
        imageBuilder.image(source)
    )
 }

 export default UrlFor