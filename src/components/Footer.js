import React from 'react'
import { NavLink } from 'react-router-dom'

const Footer = () => {
  return (
    <footer className="footer bg-gray-800">
      <div className="container relative flex items-center justify-center h-20 mx-auto px-2 sm:px-6 lg:px-8">
          <nav className="flex space-x-4">
            <NavLink className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-xl font-large san-serif tracking-widest" exact to="/">Home</NavLink>
            <NavLink className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-xl font-large san-serif" to="/about">About</NavLink>
            <NavLink className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-xl font-large san-serif" to="/projects" exact>Projects</NavLink>
            <NavLink className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-xl font-large san-serif" to="/blog" activeClassName="text-red-100 bg-red-700">Blog</NavLink>
          </nav>
      </div>
    </footer>
  )
}

export default Footer
