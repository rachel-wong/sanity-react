import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = () => {
  return (
    <header className="bg-gray-800">
      <div className="container mx-auto px-2 sm:px-6 lg:px-8">
        <div className="relative flex items-center justify-between h-20">
          <nav className="flex space-x-4">
            <NavLink className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-3xl font-large san-serif tracking-widest" activeClassName="text-red-100 bg-red-700" exact to="/">
              Home
            </NavLink>
            <NavLink className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-3xl font-large san-serif" to="/about" activeClassName="text-red-100 bg-red-700">About</NavLink>
            <NavLink className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-3xl font-large san-serif" to="/projects" exact activeClassName="text-red-100 bg-red-700">Projects</NavLink>
            <NavLink className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-3xl font-large san-serif" to="/blog" activeClassName="text-red-100 bg-red-700">Blog</NavLink>
          </nav>
        </div>
      </div>
    </header>
  )
}

export default Navbar
