import React from 'react'
import Spinner from '../assets/spinner.gif'

const Loader = () => {

  return (
    <div className="spinner-wrapper">
      <img src={Spinner} alt="Loading ... " />
      <h3 className="san-serif loading-header">Getting your content now</h3>
    </div>
  )
}

export default Loader
