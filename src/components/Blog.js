import React, { useState, useEffect } from 'react'
import sanityClient from '../client'
import { Link } from 'react-router-dom'
import { Loader } from '../components'

const Blog = () => {
  const [postData, setPostData] = useState(null)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    sanityClient.fetch(`*[_type == "post"] | order(publishedAt){
      title,
      slug,
      mainImage{
        asset->{
          _id,
          url
        },
        alt
      },
      publishedAt,
      "authorName": author->name
    }`).then(res => {
      setPostData(res)
      setLoading(false)
    }).catch(err => {
      console.error(err)
    })
  }, [])

  return (
    <>
    { loading ? <Loader /> : (
    <main className="min-h-screen p-12 blog-container">
      <h2 className="serif text-lg flex text-xl4 blog-heading">Blog</h2>
      <section className="blog-grid">

        {postData && postData.map((post, idx) => (
          <article key={idx} className="blog-panel article w-full">
            <Link className="blog-panel-link" to={"/blog/" + post.slug.current} key={post.slug.current} >
              <div className="blog-panel-image-wrapper">
                <img src={post.mainImage.asset.url} alt={post.title} />
              </div>
              <h3 className="serif blog-panel-title">{post.title}</h3>
              <div className="blog-panel-details serif">
                <p className="">{post.authorName ? post.authorName : ("Anon")}</p>
                <p className="">{ new Date(post.publishedAt).toDateString()}</p>
              </div>
              <a href={"/blog/" + post.slug.current} className="san-serif blog-panel-button">Read more</a>
            </Link>
          </article>
        ))}
        </section>
        </main>
      )}
    </>
  )
}

export default Blog
