import sanityClient from '@sanity/client'

export default sanityClient({
  projectId: "y3u1a6rx",
  dataset: "production"
})