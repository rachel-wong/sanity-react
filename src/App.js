import React from 'react';
import { Home, Projects, Project, Navbar, About, Blog, Footer,SinglePost, NotFound } from './components'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

function App() {
  return (
    <div className="flex flex-col min-h-screen flex-grow">
      <Router>
        <Navbar />
        <Switch>
          <Route path="/" exact component={ Home } />
          <Route path="/about" component={ About } />
            <Route path="/projects" exact component={Projects} />
            <Route path="/projects/:projectslug" component={Project} />
          <Route path="/blog/:slug" component={ SinglePost } />
          <Route path="/blog" component={ Blog } />
          <Route path="/404" component={NotFound} />
          <Redirect from='*' to='/404' />
        </Switch>
        <Footer/>
      </Router>
    </div>
  );
}

export default App;
