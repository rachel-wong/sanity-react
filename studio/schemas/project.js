export default {
  name: 'project',
  title: 'Project',
  type: 'document',
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
      validation: Rule => [
    Rule.required().min(10).error('A title of min. 10 characters is required'),
    Rule.max(50).warning('Shorter titles are usually better')
  ]
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'title',
        maxLength: 96,
      },
    },
    {
      name: 'author',
      title: 'Author',
      type: 'reference',
      to: { type: 'author' },
    },
    {
      name: 'mainImage',
      title: 'Main image',
      type: 'image',
      options: {
        hotspot: true,
      },
    },
    {
      name: 'blurbImage',
      title: 'Blurb image',
      type: 'image',
      options: {
        hotspot: true,
      },
      validation: Rule => [Rule.required()]
    },
    {
      name: 'projectType',
      title: 'Project Type',
      type: 'string',
      validation: Rule => [
    Rule.required()],
      options: {
        list: [
          {
            value: 'personal',
            title: 'Personal',
          },
          {
            value: 'commercial',
            title: 'Commercial'
          },
          {
            value: 'nonprofit',
            title: 'Not for profit'
          }
        ]
      }
    },
    {
      name: 'link',
      type: 'url'
    },
    {
      name: 'tags',
      type: 'array',
      of: [
        {
          type: 'string',
        }
      ],
      options: {
        layout: 'tags'
      }
    },
    {
      name: 'publishedAt',
      title: 'Published at',
      type: 'datetime',
      validation: Rule => [Rule.required()]
    },
    {
      name: 'body',
      title: 'Body',
      type: 'blockContent',
    },
  ],

  preview: {
    select: {
      title: 'title',
      author: 'author.name',
      media: 'blurbImage',
    },
    prepare(selection) {
      const {author} = selection
      return Object.assign({}, selection, {
        subtitle: author && `by ${author}`,
      })
    },
  },
}
