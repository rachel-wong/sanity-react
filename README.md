## Sanity && React

> Testing Headless CMS Sanity

![screenshot of Sanity blog](/src/assets/blog-screenshot.gif)

**Dependencies**

* Sanity CLI (global)
* sanity/client (to manage connecting up to sanity headless cms instance)
* @sanity/block-content-to-react (to render out blocks of body text)
* @sanity/image-url (to attach additional cropping attributes to any images)

Sanity set up commands in terminal

`sanity login`

`sanity start`

Added tailwindcss as a link CDN resource instead of npm package

### Points of note

* When developing, need to run both the react server `npm start` as well as the sanity backend server `sanity start` inside the folder. The Sanity backend can be initiated inside the same react project (`/studio` in this case)
* To render rich text body fields from Sanity, `BlockContent` is needed from `@sanity/block-content-to-react`
* Image rendering can make use of the `@sanity/image-url` package. See `singlePost.js` component.
* Connecting to the headless CMS endpoint requires a `client.js` using `@sanity/client`. Use the sanity client to make fetch api calls using GROQ queries
* Test run GROQ queries in the backend using the `Vision` function
* New content types needs to be created as a new schema inside the sanity project `/schema` folder
* ProjectId is needed to establish a connection using the sanity client. It can be found in account dashboard manage.sanity.io or in `sanity.json`. The projectId is also used in components like `BlockContent`
* `activeClassName` can be added `NavLink` to apply css classes when user has actually navigated to the route
* Headless CMS speeds up development significantly without having too much local environment set up

### Problems encountered

* noticeable loading time even for api responses even for low content data. Have to put in a loader for every page
* Multiple network calls to be made - one per page. Not an ideal set up given the nature of the website actually doesn't require that much network load.
* mixed up third-level routes with different slugs between /projects and /blog.
* Errors continued to block netlify deploy multiple times.
* Gave up on the tailwindcss because too many in-line classes added to the HTML, which detracted from the focus on how to use the sanity.io api to pull in data.
* Website projects of this calibre benefits from a clear delineation (folder structure, logic, naming conventions) between "pages" and "components" (i.e. project page as oppose to project component)

#### Deployment

* normal netlify deployment for the react front-end
* deploy the sanity backend

`sanity deploy`

1. Assign a hostname which will deploy to https://whateverhostnameyouchose.sanity.studio/desk/post
This will deploy the backend which once logged in, can update the site with new content

2. Both the deployed front/back end addresses needed to be added to the CORS origin in the Sanity dashboard. This is done to the project in https://manage.sanity.io/ > Settings > API

3. need to add a `_redirect` file to the /public folder root to point to `index.html`

Current deployed backend for this project: https://rachelsanityreact.sanity.studio/desk/post

Current deployed frontend for this project: https://modest-euclid-01d9c6.netlify.app/